/**
 * author: Enyonam
 * Date: 24/08/19
 */

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import static org.openqa.selenium.By.name;


public class GoToGooglePage {

    @Test
    public void launchGoogleTest() {

        String pathToDriver = "src\\main\\resources\\chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", pathToDriver);//setup driver path
        WebDriver driver = new ChromeDriver();//instantiate Chrome Driver
        WebDriverWait wait = new WebDriverWait(driver, 30);

        driver.get("https://www.google.com");//open url.

        //identifying search bar and typing in Samuel Eto'o
        driver.findElement(By.xpath("//input[@name='q']")).sendKeys("Samuel Eto'o");

        //Identifying search button and clicking it to run search
        driver.findElement(By.xpath("(//input[@value='Google Search'])[2]")).click();

        //validate search was successful
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(), \"Samuel Eto'o - Wikipedia\")]")));

        driver.close();//close browser
    }
}
